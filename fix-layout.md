# Note for fixing layout in ghc wiki

## Fix definition lists

Just modify the definition list into an item list.

### Example

* Old Trac
  * https://trac.haskell.org/trac/ghc/wiki/Commentary/CodingStyle#TheCPreprocessorCPP
* Migrated GitLab Wiki
  * https://gitlab.haskell.org/ghc/ghc/wikis/commentary/coding-style?version_id=11d8ff45310b19d70a74aefc05721581f4321552#the-c-preprocessor-cpp


### Fixing

Old Trac:
```
 '''DEBUG''':: 
  Used to enables extra checks and debugging output in the compiler. The ASSERT macro (see {{{HsVersions.h}}}) provides assertions which disappear when DEBUG is not defined. 
```

Migrated GitLab wiki:
```
<table><tr><th>**DEBUG**</th>
<td>
Used to enables extra checks and debugging output in the compiler. The ASSERT macro (see `HsVersions.h`) provides assertions which disappear when DEBUG is not defined. 
</td></tr></table>
```

How to fix:
```
- **DEBUG**

  Used to enables extra checks and debugging output in the compiler. The ASSERT macro (see `HsVersions.h`) provides assertions which disappear when DEBUG is not defined. 
```


## Fix block quotes

Just fix the indent to the same level.
See [gitlab document](https://gitlab.haskell.org/help/user/markdown#lists).

### Example

* Old Trac
  * https://trac.haskell.org/trac/ghc/wiki/Commentary/CodingStyle#TabsvsSpaces
* Migrated GitLab Wiki
  * https://gitlab.haskell.org/ghc/ghc/wikis/commentary/coding-style?version_id=11d8ff45310b19d70a74aefc05721581f4321552#tabs-vs-spaces
  
### Fixing

Old Trac:
```
  * In !TextMate, in the tabs pop-up menu at the bottom of the window, select "Soft Tabs", as show in the following screenshot where the blue rectangle is:

 Alternatively, open the Bundle Editor and add a new Preference called Indentation to the bundle editor. Give it the following contents:
```

Migrated GitLab wiki:
```
- In TextMate, in the tabs pop-up menu at the bottom of the window, select "Soft Tabs", as show in the following screenshot where the blue rectangle is:

>
> Alternatively, open the Bundle Editor and add a new Preference called Indentation to the bundle editor. Give it the following contents:
```

How to fix:
```
- In TextMate, in the tabs pop-up menu at the bottom of the window, select "Soft Tabs", as show in the following screenshot where the blue rectangle is:

  Alternatively, open the Bundle Editor and add a new Preference called Indentation to the bundle editor. Give it the following contents:
```


